from rest_framework import serializers
from .models import *
#from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password


class societySerializer(serializers.ModelSerializer):
    class Meta:
        model = Society
        fields  = '__all__'
        exclude = ['password']


class visitorSerializer(serializers.ModelSerializer):
	society = serializers.SlugRelatedField(read_only=True, slug_field='society_user')
	
	#print(society)
	class Meta:
		model = Visitor
		fields = ['id','vname','phone','vehicle','society','wing','flat_no','in_time','purpose','out_time']
	

class RegisterSerializer(serializers.ModelSerializer):
    society_user = serializers.CharField(write_only=True, required=True)
    email = serializers.EmailField(required=True,validators=[UniqueValidator(queryset=Society.objects.all())])
    name = serializers.CharField(write_only=True, required=True)
    password1 = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = Society
        fields = ['society_user', 'email','name','password1', 'password2']

    def validate(self, attrs):
        if attrs['password1'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        society = Society.objects.create(
			society_user=validated_data['society_user'],
			email=validated_data['email'],
			password=validated_data['password1'],
            name=validated_data['name'],
			# last_name=validated_data['last_name']
		)

		
        society.set_password(validated_data['password1'])
        society.save()

        return society