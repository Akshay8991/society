# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 22:27:36 2021

@author: aksha
"""

from .models import *

class SocietyAuthBackend():
    def authenticate(self,request,username,password):
        try:
            user = Society.objects.get(society_user=username)
            success = user.check_password(password)
            
            if success:
                return user
            
        except Society.DoesNotExist:
            pass
        return None
    
    
    def get_user(self,sid):
        try:
            return Society.objects.get(pk=sid)
        except:
            return None