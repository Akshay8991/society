from django.db import models
from django.contrib.auth.models import User
from PIL import Image
from django.contrib.auth.models import Group
from django.conf import settings
from django.utils.text import slugify
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,BaseUserManager


class CustomSocietyManager(BaseUserManager):
    def create_society(self,society_user,name,email,password,**other_fields):
        if not society_user:
            raise ValueError(_('You must provide Society User'))
        email = self.normalize_email(email)
        society = self.model(society_user=society_user,name=name,email=email,**other_fields)

        society.set_password(password)
        society.save()
        return society

    def create_superuser(self,society_user,name,email,password, **other_fields):
        other_fields.setdefault('is_staff',True)
        other_fields.setdefault('is_superuser',True)
        other_fields.setdefault('is_active',True)


        if other_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must be assigned to is_staff=True.'
            )

        if other_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must be assigned to is_superuser=True.'
            )

        return self.create_society(society_user,name,email,password, **other_fields)
    

class Society(AbstractBaseUser,PermissionsMixin):
    society_user = models.CharField(max_length=200,unique=True)
    email = models.EmailField(_('email address'),unique=True)
    name = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    address = models.TextField(_('address'),max_length=500,blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    objects = CustomSocietyManager()
    USERNAME_FIELD = 'society_user'
    REQUIRED_FIELDS = ['email','name']


        
    def __str__(self):
        return self.society_user
    








class Visitor(models.Model):


    VEHICLE = (
        ('None','No vehicle'),
        ('Bike','Bike'),
        ('Car','Car')
    )




    
    vname = models.CharField(max_length =200,null=True)
    phone = models.CharField(max_length =200,null=True,blank=True, help_text='Contact phone number')
    vehicle = models.CharField(max_length =200,null=True,choices =  VEHICLE)
    in_time = models.DateTimeField(auto_now_add =True,blank=True,null=True)
    society = models.ForeignKey(Society,null=True, blank =True,on_delete=models.CASCADE)
    wing = models.CharField(max_length =200,null=True)
    flat_no = models.CharField(max_length =200,null=True)
    purpose = models.CharField(max_length =200,null=True)
    out_time = models.DateTimeField(blank=True,null=True)
    
    

   
    def __str__(self):
        return self.vname
    



