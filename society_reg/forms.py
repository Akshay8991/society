from django.forms import ModelForm
from .models import *
from django.contrib.auth.forms import UserCreationForm,UsernameField
from django import forms
#from django.contrib.auth.models import User


class CreateSocietyForm(UserCreationForm):
	class Meta:
		model = Society
		fields = ['society_user','name','email','password1','password2']
	

	

class CreateVisitorForm(ModelForm):
	class Meta:
		model = Visitor
		fields = '__all__'
		

class UpdateVisitorForm(ModelForm):
	class Meta:
		model = Visitor
		fields = '__all__'
		# exclude = ['in_time']



# class FlatForm(ModelForm):

# 	class Meta:
# 		model = Flat
# 		fields = '__all__'

# 		exclude = ['society']
