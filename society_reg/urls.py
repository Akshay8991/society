from . import views
from django.urls import path
from django.conf.urls import url
 


urlpatterns = [
    path('',views.home,name="home"),
    path('registration/',views.register,name="reg"),
    path('society/',views.society),
    path('visitorList/', views.visitorList),
    path('userList/', views.societyList),
    path('register/', views.RegisterView.as_view(), name='auth_register'),
    path('visitordetails/<int:pk>/', views.visitordetails),
    path('login/', views.login_view,name='login'),
    path('logout/',views.logout_view,name="logout"),
    path('user/',views.userpage,name="user"),
    path('visitor_info/<int:pk>/',views.visitor_info,name="visitor_info"),
    path('update_visitor/<int:pk>/',views.updatevisitor,name="updatevisitor"),
    # path('add_flat/<int:pk>/',views.addFlat,name="addflat"),
    path('add_visitor/',views.add_visitor,name="add_visitor"),

] 
