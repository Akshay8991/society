from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin
from django.forms import TextInput,Textarea



class SocietyAdminConf( UserAdmin):
    search_fields = ('society_user','email','name')
    list_filter = ('society_user','email','name','is_active','is_staff')
    ordering = ('-created_date',)
    list_display = ('society_user','email','name','password','is_active','is_staff')


    fieldsets = (
        (None,{'fields':('society_user','email','name','password')}),
        ('Permissions',{'fields':('is_active','is_staff')}),
        ('Personal',{'fields':('address',)}),
        ('Group',{'fields':('groups',)}),
    )
    

    formfield_overrides = {
            Society.address : {'widget': Textarea(attrs={'rows': 10,'cols': 40})},
            }
        
    
    add_fieldsets = (
            (None,{
                'classes':('wide',),
                'fields':('society_user','name','email','address','password1','password2','is_active','is_staff')}
                ),
        )


admin.site.register(Society,SocietyAdminConf)













admin.site.register(Visitor)

#admin.site.register(Wing)
#admin.site.register(Flat)


admin.site.site_header = "Society Management"
