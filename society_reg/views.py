from django.shortcuts import render,redirect
from .models import *
from django.http import HttpResponse
from .forms import CreateSocietyForm,CreateVisitorForm,UpdateVisitorForm
#from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from .serializers import visitorSerializer,RegisterSerializer, societySerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django import template
from rest_framework.decorators import api_view
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated,AllowAny
from django.contrib.auth import authenticate,login,logout
from .decorators import unauthenticated_user,allowed_user,admin_only
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def home(request):
	visitors = Visitor.objects.all()
	
	context = {'visitors':visitors}
	if request.user.is_staff:	
		return render(request,'society_reg/home.html',context)
	else:
		return redirect('user')



def register(request):

	form =  CreateSocietyForm()
	if request.method == 'POST':
		form =  CreateSocietyForm(request.POST)
		#print(dir(form))
		if form.is_valid():

			user = form.save()
			username = form.cleaned_data.get('username')
		
			messages.success(request,"Account was created for " + username)

			

			return redirect('home')
	context = {'register':form}
	return render(request,'society_reg/registration.html',context)


def visitor(request):
    visitors = Visitor.objects.all()

    context = {'visitors':visitors}

    return render(request,'society_reg/visitor.html',context)

@login_required(login_url='login')
@api_view(['GET', 'POST'])
def visitorList(request):

	print(request.user)
	if request.method == 'GET':
		visitors = request.user.visitor_set.all()
		serializer = visitorSerializer(visitors,many=True)
		
		return Response(serializer.data)

	elif request.method == 'POST':
		
		serializer = visitorSerializer(data=request.data)

		if serializer.is_valid():
			serializer.save()

		return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@login_required(login_url='login')
@api_view(['GET'])
def society(request):
     if request.method == 'GET':
         society = Society.objects.all()
         serializer = societySerializer(society,many=True)
		
         return Response(serializer.data)

     elif request.method == 'POST':
		
        serializer = societySerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
             
@login_required(login_url='login')             
@api_view(['GET'])
def societyList(request):

    if request.method == 'GET':
        print(dir(request.user))
        content = {
			'id': str(request.user.id),
			'user': str(request.user),
			'password': str(request.user.password),
			'auth': str(request.auth),  # None
		}
        return Response(content)



class RegisterView(generics.CreateAPIView):
    queryset = Society.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer




@api_view(['GET', 'PUT', 'DELETE'])
def visitordetails(request,pk):
		try:
			visitors =  Visitor.objects.get(id = pk)
		except Visitor.DoesNotExist:
			return Response(status=status.HTTP_404_NOT_FOUND)

		if request.method == 'GET':
			serializer =  visitorSerializer(visitors)
			return Response(serializer.data)

		elif request.method == 'PUT':
			serializer = visitorSerializer(visitors, data=request.data)
			if serializer.is_valid():
				serializer.save()
				return Response(serializer.data)
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

		elif request.method == 'DELETE':
			visitors.delete()
			return Response(status=status.HTTP_204_NO_CONTENT)
		

@unauthenticated_user
def login_view(request):
    print(dir(Society))
    if request.method == 'POST':
        society_user = request.POST.get('society_user')
        password = request.POST.get('password')
 
    
        user = authenticate(request,username=society_user,password=password)

        if user is not None:
          
            login(request, user)
            return redirect('home')
        else:
            messages.info(request,'Username OR password is incorrect')



    context = {'messages':messages}
    
    

    return render(request,'society_reg/login.html',context)


def logout_view(request):
	logout(request)

	return redirect('login')



@login_required(login_url='login')
@allowed_user(allowed_roles=['Society'])
def userpage(request):

	visitors = request.user.visitor_set.get_queryset().order_by('id')
	print(request.user.visitor_set)

	#visitors = Visitor.objects.all()


	

	total_visitors = visitors.count()

	

	page = request.GET.get('page', 1)

	paginator = Paginator(visitors, 10)
	try:
	    visitors = paginator.page(page)
	except PageNotAnInteger:
		visitors = paginator.page(1)
	except EmptyPage:
		visitors = paginator.page(paginator.num_pages)


	context = {'visitors':visitors,'total_visitors':total_visitors}
	return render(request,'society_reg/society.html',context)


def visitor_info(request,pk):

	#cust = request.user.customer
	#form = CustomerForm(instance=cust)


	visitors = Visitor.objects.get(id = pk)
	#orders = customer.order_set.all()
	#total_orders = orders.count()

	#print(customer.profile_pic)


	context = {'visitors':visitors }
	return render(request,'society_reg/visitor_info.html',context)


def updatevisitor(request,pk):
		#visitors = Visitor.objects.get(id=pk)
		society = request.user.visitor_set.get(id=pk)
		print(request.user)
		form = UpdateVisitorForm(instance=society)
		if request.method == "POST":
		#print('Printing Post',request.POST)
			
			form = UpdateVisitorForm(request.POST)
			if form.is_valid():
				print(society.vname)
				form.save()
				return redirect('home')
		context = {'form':form,'visitors':society}	
		return render(request,'society_reg/update_visitor.html',context)



# @login_required(login_url='login')
# @allowed_user(allowed_roles=['Society'])
# def addFlat(request,pk):
# 	society = Society.objects.get(id=pk)
# 	form = FlatForm(instance=society)

	
# 	if request.method == 'POST':
# 		form = FlatForm(request.POST,instance=society)
# 		if form.is_valid():
# 			form.save()
# 			return redirect('home')

# 	context = {'form':form }

# 	return render(request,'society_reg/add_flat.html',context)


@login_required(login_url='login')
@allowed_user(allowed_roles=['Society'])

def add_visitor(request):
    #print(request.user)
    #society = Society.objects.filter(society_user=request.user)
    #print(society)
    form = CreateVisitorForm()
    
    
    if request.method == 'POST':
        form = CreateVisitorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('user')

    context = {'form':form }

    return render(request,'society_reg/add_visitor.html',context)
